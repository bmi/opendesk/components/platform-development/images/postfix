# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0
FROM docker.io/alpine:3.21.2@sha256:56fa17d2a7e7f168a043a2712e63aed1f8543aeafdcee47c58dcffe38ed51099 AS release

LABEL org.opencontainers.image.authors="Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH" \
      org.opencontainers.image.documentation=https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/postfix/-/blob/main/README.md \
      org.opencontainers.image.source=https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/postfix\
      org.opencontainers.image.vendor="Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH" \
      org.opencontainers.image.licenses=Apache-2.0 \
      org.opencontainers.image.base.name=docker.io/alpine:3.21.2 \
      org.opencontainers.image.base.digest=sha256:56fa17d2a7e7f168a043a2712e63aed1f8543aeafdcee47c58dcffe38ed51099

RUN apk add --no-cache \
    bash=5.2.37-r0 \
    ca-certificates=20241121-r1 \
    postfix=3.9.1-r0 \
    postfix-pcre=3.9.1-r0 \
    cyrus-sasl=2.1.28-r8 \
    cyrus-sasl-crammd5=2.1.28-r8 \
    cyrus-sasl-digestmd5=2.1.28-r8 \
    cyrus-sasl-login=2.1.28-r8 \
    cyrus-sasl-ntlm=2.1.28-r8

COPY postfix-service.sh /
RUN chmod +x /postfix-service.sh

VOLUME ["/var/spool/postfix"]

ENTRYPOINT ["/postfix-service.sh"]

EXPOSE 25
EXPOSE 587
