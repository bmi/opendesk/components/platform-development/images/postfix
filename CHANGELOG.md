## [3.0.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/postfix/compare/v3.0.0...v3.0.1) (2025-02-18)


### Bug Fixes

* **postfix:** Add cyrus-sasl ([8341d7a](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/postfix/commit/8341d7ad76e130e277a3d039264ce1d823757266))

# [3.0.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/postfix/compare/v2.0.0...v3.0.0) (2025-02-12)


### Bug Fixes

* **Dockerfile:** Update components, remove overrides, change entrypoint ([af05b92](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/postfix/commit/af05b923861098521bd804d86307ef57fdcc7cec))


### BREAKING CHANGES

* **Dockerfile:** Move override functionality into Helm chart

# [2.0.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/postfix/compare/v1.1.0...v2.0.0) (2024-08-07)


### Features

* Use static default configuration which replace instable env var ([f405093](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/postfix/commit/f405093679d181e8df7f156d253c21b8f73b9dc5))


### BREAKING CHANGES

* Remove environment variable substitution

# [1.1.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/postfix/compare/v1.0.0...v1.1.0) (2024-08-06)


### Features

* **Dockerfile:** Update components, add dkim milter variable ([3519654](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/postfix/commit/35196541fdf33790006f52cb233e1e04e9eda249))

# 1.0.0 (2023-12-27)


### Bug Fixes

* Lookup internal k8s services without fqdn ([051e681](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/postfix/commit/051e68148d82deb9fbe0971ba0494a01dc33c707))
* Postfix uses lmdb by default ([5acbf41](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/postfix/commit/5acbf4186e62f82986d4b1f20d2980ef5155caa2))
* readme ([f60764f](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/postfix/commit/f60764f7bff419aa53d99fe572c051e9c05f7050))
* removed AMAVIS_HOST, use content_filter ([7bedce9](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/postfix/commit/7bedce934d46be14a6e573e68d91e56a08e64168))
* set better defaults ([fdc1297](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/postfix/commit/fdc12975fa43fc83e45c100c9bfb744d94991f9f))
* set virtual_transport to "virtual" as default ([9029d1a](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/postfix/commit/9029d1a5ce044085a0f26e53cf1b6daf1395fe6c))


### Features

* add smtpd milters support ([2f553e7](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/postfix/commit/2f553e736846dd7ab6d0f352e572ea34aa0e610e))
* Initial commit ([a5acd0e](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/postfix/commit/a5acd0ee20974de61de9e095c0ca61312dbf90d4))
* **postfix:** add support for smtp_host_lookup ([40c3f44](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/postfix/commit/40c3f44d7f01676124735df13d920cd1e6af54e5))
<!--
SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
