<!--
SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
# Postfix Docker Image

This image provides a blank postfix installation. It is based on https://github.com/interlegis/alpine-postfix


## TL;DR

After building the image should be available here:
```
registry.opencode.de/bmi/opendesk/components/platform-development/images/postfix
```

# License

This project uses the following license: Apache-2.0

# Copyright

Copyright (C) 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
