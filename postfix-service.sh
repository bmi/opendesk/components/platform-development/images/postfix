#!/bin/bash
# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

function log {
  echo `date` $ME - $@
}

function serviceStart {
  log "[ Starting Postfix... ]"
  /usr/sbin/postfix start-fg
}

serviceStart &>> /proc/1/fd/1
